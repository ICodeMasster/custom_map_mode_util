# custom map mode utils
Created by Icodemaster for easy of custom map mode addition to HOI4
## Supported Features
- Supports easy implementation with a single effect and filter name string
- Add unique filters to specific countries, or open the mapmode with only certain filters allowed
- Allows generation of any number of colors to use for mapmodes!
- Easy configuration options and portable .exe
- Supports country shields and country-level coloring
- Add tertiary text to states on the map with a simple flag and text string
- Hashed state overlays to display 2 types of data at once
- Supported scaling of state images to save memory and mod size
- Create button effects that run in state scope
- Add tooltips to states in state scope
- Disable/grey buttons when not neede
- Change buttons to click-through on state-by-state basis

## Requirements
Please download the latest release of the geoparser from below. Edit the config file as needed and run the exe.
https://github.com/ICodeMaster/hoi4geoparser/releases

## Planned Features
- Reimplimenting zoom buttons

## Quickstart Guide

1. Set this mod as a dependency for your vanilla mod, or copy the contents of this mod into your mod.

2. Add an on_action in common/on_actions with an on_startup like below:
on_startup = {
    effect = {
        1 = {
            add_to_array = { global.filter_arrays = [YOUR FILTER INDEX] }
        }
    }
}

3. Localize your filter in a localisation file
custom_map_mode_filter_name_[YOUR FILTER INDEX]:0 "My Custom Filter"[/code]

3.5 Localize state tooltips if you would like!
custom_map_mode_state_tooltip_[YOUR FILTER INDEX]:0 "Filter State Tooltip"
custom_map_mode_state_tooltip_delayed_[YOUR FILTER INDEX]:0 "Filter State Delayed Tooltip"


4. Populate your filter using a scripted effect in common/scripted_effects:

custom_map_mode_filter_initialize_[YOUR FILTER INDEX] = {
  find_color_graph = yes
  for_each_scope_loop = {
      array = global.states
      OWNER = { set_variable = { PREV.state_frame_number = custom_map_color } }
      set_state_flag = mapmode_state_visible
  }
}

4.5 Add a button effect:

custom_map_mode_filter_click_[YOUR FILTER INDEX] = {
	ROOT = {
		launch_nuke = {
			state = PREV
			controller = ROOT
			use_nuke = no
		}
	}
}

This example creates a color-graph for countries making sure no two bordering nations share a color. If you click on a state, it will nuke a random province!

To overwrite the filters in the example, it is recommended you only remove/edit the files named example_filters.

## Geoparser Quickstart
1. Download the geoparser from https://github.com/ICodeMaster/hoi4geoparser/releases

2. Put it in any folder you would like. Run it once to generate the config

3. Edit the modPath and hoi4Path to be their respective paths

4. Add colors! The first color block defines the state-colors for the "solid" style. RGBA values between 0-255 only.
    4.5 The second block allows defining "hashed" style colors.

5. The first time you use start the mod, use this console command: "effect find_color_graph" This will output the minimum number of "solid" style colors you need to have in your config to color every country on your map so no two countries share a color. Vanilla requires 5 colors

6. Run the tool again. When prompted, generate the files for the mapmode mod

7. Copy the example mod into your mod path.

8. Copy the contents of the mod_path directory in the tool's directory into your mod path.

9. All done! Create and modify filters as desired.

## Function Reference:
global.states -> All of the states in the map function

find_color_graph = yes -> Creates country colors so no neighbors share a color

assign_owner_color_to_state = yes -> Use Owner color for states

assign_controller_color_to_state = yes -> Use Controller color for states

set_state_flag = mapmode_state_hashed_visible -> State has hashed texture

set_state_flag = mapmode_state_visible -> State has normal texture visible

state_frame_number_hashed -> Frame (Color) for hashed state texture

state_frame_number -> Frame (Color) for normal state texture

calculate_country_center_point_quick_all = yes -> Find the rough centerpoint of every nation

set_country_flag = mapmode_shield_visible -> Show shield for a nation (Country Scope)

calculate_country_shield_capital_all = yes -> Use capital state for country flag instead of centerpoint

set_state_flag = info_text_visible -> Show info text

set_state_flag = custom_mapmode_tooltip -> Show tooltip on hover

set_state_flag = custom_mapmode_tooltip_delayed -> Show delayed tooltip on hover

enable_custom_mapmode_state_buttons = yes -> Enable all states as buttons

set_state_flag = custom_map_state_is_button -> Single state button flag (Do not use with effect above)

disable_custom_mapmode_state_buttons = yes -> Disable all states as buttons

set_state_flag = mapmode_state_click_disabled -> Gray out state and make it unclickable. Will no longer show highlight effect.

set_country_flag = custom_map_mode_open_button_visible -> Show the independent open button outside of bespoke mapmode open calls.

open_custom_mapmode = yes -> Open the map gui

temp_filter_to_open -> Temp variable used with open effects to specify filter on open.

set_country_flag = custom_map_mode_open_button_visible -> Show the independent open button outside of bespoke mapmode open calls.

open_custom_mapmode_global_filters = yes -> Open map GUI with global filters and registered country-specific filters

global.filter_arrays -> Array with global filters. Visible to all nations.

country_specific_filter_array -> Country Array with nation-specific filters. Set on a per-country-scope basis

open_custom_mapmode_filter_limited = yes -> Open map gui with filters in temp_array: temp_arrays_to_register

temp_arrays_to_register -> Temp array used to add specific filters with the open_custom_mapmode_filter_limited effect

set_country_flag = custom_map_mode_filter_change_disabled -> Country flag set to remove ability to click the filter change button.

set_country_flag = custom_map_mode_custom_filter_header -> Use custom header for this filter